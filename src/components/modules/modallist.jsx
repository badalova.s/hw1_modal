export const modalWindowDeclarations = [
    {
      id: '1',
      title: "Do you want delete this file?",
      description: 'Once you delete this file, it won`t be possible to undo this action. Are you sure yo want to delete it?',
      button:"OK",
    },
    {
      id: '2',
      title: "Do you want save this file?",
      description: 'Are you sure yo want to save this file?',
      button:"Save",
    }
  ]