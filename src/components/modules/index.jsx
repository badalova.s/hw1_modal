import { Component } from "react";
import styles from "./modal.css";

export default class Modal extends Component {
    constructor(props) {
      super(props);
    }
 
    

    render() {
        
       return (
        
        <div className={this.props.active ? "modal active" : "modal"} onClick={this.props.Click}>
        
        <div className={this.props.active ? "modal_content active" : "modal_content"} onClick={e=>e.stopPropagation()}>
        <div className="modal_header">
        <span>{this.props.header}</span>
        <span onClick={this.props.Click}>X</span>
        
        </div>
        <p>{this.props.text}</p>
        <div className="modal_btn_conteiner">
        <button className="modal_btn">{this.props.actives}</button>
        </div>
        </div>
        </div>
      );
    }
  }
  