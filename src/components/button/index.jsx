import { Component } from "react";
// import Modal from "../components/modal";

import styles from "./button.css";
export let modalDeclaration
export default class Button extends Component {
  constructor(props) {
    super(props);
   this.state={show:props.show}
  }
  render() {

    return (
      <button className="btn" style={{
        backgroundColor:`${this.props.background}`,
          width: "100px",
          height: "50px"
    }}
        data-modal={this.props.data}

        
        onClick={this.props.Click
                   }
      >
        {this.props.text}
      </button>
    );
  }
}
