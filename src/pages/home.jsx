import { Component } from "react";
import Button from "../components/button";
import Modal from "../components/modules";
import {modalWindowDeclarations} from "../components/modules/modallist"

export class Home extends Component {
    constructor(props) {
        super(props);
    this.state={}
    }
    setActive=(active)=>{
        this.setState({modalToShow:active})
    }
   
  render() {
    return (
      <>
      <div>
        <Button background="blue" text="Open first modal" data="1" Click={(event)=> {let modalID = event.target.dataset.modal;
            let modalDeclaration = modalWindowDeclarations.find(item => item.id ==modalID);
            this.setActive(true)
            this.setState({modalState:modalDeclaration.title,
                modalText:modalDeclaration.description,
                modalButton:modalDeclaration.button
             } )

            
                   }}  />
        <Button background="yellow" text="Open second modal" data="2" Click={(event)=> {let modalID = event.target.dataset.modal;
            let modalDeclaration = modalWindowDeclarations.find(item => item.id ==modalID);
            this.setActive(true)
            this.setState({modalState:modalDeclaration.title,
                modalText:modalDeclaration.description,
                modalButton:modalDeclaration.button
            })

            
                   }}  />
        </div>
        <Modal header={this.state.modalState} active={this.state.modalToShow} 
        text={this.state.modalText}
        actives={this.state.modalButton}

        Click={(e)=> this.setActive(false)}
         />
      </>
      
    );
  }
}
